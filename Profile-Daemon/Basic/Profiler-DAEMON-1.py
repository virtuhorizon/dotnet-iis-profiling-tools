# -*- coding: utf-8 -*-
# coding: utf-8

# In[ ]:

# define and create directories

# cache location

# program locatioons for procmon, 7z

#configuration files

# timing variables 

#

#init procmon with configuration

#loop untill timer out

# export .PML and XML

# kill procmon

#7z results

# purge cache

#Loop


# In[5]:

import os, glob, sys
import time
import datetime
import subprocess

DEBUG = True
# define and create directories

try:
    PATH_CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
except NameError:  # We are the main py2exe script, not a module
    PATH_CURRENT_DIR = os.path.dirname(os.path.abspath(sys.argv[0]))


PATH_WORKING_DIR = os.path.join(PATH_CURRENT_DIR,"_ProfilingDir") # cache location
#PATH_LOG = os.path.join(PATH_CURRENT_DIR,"profiling_errors.log")

# program locatioons for procmon, 7z

#configuration files
PATH_PROCMON = os.path.join(PATH_CURRENT_DIR,"_ProfilingBin","procmon.exe")
PATH_7z = os.path.join(PATH_CURRENT_DIR,"_ProfilingBin","7za.exe")
PATH_config = os.path.join(PATH_CURRENT_DIR,"_ProfilingBin","procmon_config.pmc")
PATH_DEL = os.path.join("C:\Windows\System32","del.exe")

# timing variables 
#time to run
TIME_LOOP = 60 #minutes
#wait 1
TIME_export = 5 #minutes
#wait 2
time_compress = 5 #minutes

time_multiply = 60 # usually 60 is one minute
def get_time_name():
    return datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') # this is a valid folder name too

def deletion_macro():
    notDeleted = True
    while(notDeleted):
        time.sleep(time_compress * time_multiply)
        try:
            if DEBUG: print "+ delete cache try"
            for filename in glob.glob(os.path.join(PATH_WORKING_DIR,"*.pml")):
                os.remove( filename )
            notDeleted = False
        except:
            if DEBUG: print "+ delete cache locked"
            pass
        
def procmon_loop():
    #call(["ls", "-l"])
    TIME_STARTED = get_time_name()
    profiling_name = "profiling_"+TIME_STARTED
    cacheFile = os.path.join(PATH_WORKING_DIR, profiling_name)
    
    if DEBUG: print "+ init profiling"
        
    subprocess.Popen([PATH_PROCMON, "/AcceptEula","/Quiet","/BackingFile", cacheFile+".pml"])
    if DEBUG: print "+ profiling"
        
    time.sleep(TIME_LOOP * time_multiply)
    if DEBUG: print "+ killing"
        
    subprocess.Popen([PATH_PROCMON, "/AcceptEula","/Quiet","/Terminate"])
    #if DEBUG: print "d"
    time.sleep(10) #seconds
        
    #subprocess.Popen([PATH_PROCMON, "/AcceptEula","/openLog",cacheFile+".pml","/SaveAs",cacheFile+".xml"])
    #if DEBUG: print "e"
        
    time.sleep(TIME_export * time_multiply)
    #if DEBUG: print "f"
    if DEBUG: print "+ init compression"
    subprocess.Popen([PATH_7z,"a","-t7z",cacheFile+".7z",os.path.join(PATH_WORKING_DIR,"*.xml"),os.path.join(PATH_WORKING_DIR,"*.pml")])
    if DEBUG: print "+ wait compression"
    deletion_macro()
    
    if DEBUG: print "+ loop"

#init procmon with configuration
## procmon.exe /AcceptEula  /Quiet /BackingFile test_cache.pml /LoadConfig ProcmonConfiguration.pmc
##timeout

## procmon.exe /AcceptEula /Quiet /Terminate


## procmon.exe /AcceptEula  /Quiet /openLog test_cache.pml /SaveAs test1.xml
## wait 5 minutes

## 7za.exe a -t7z NOWtime.7z *.xml

## wait 5 minutes


## delete *.pml
## delete *.xml

#Loop

while (True):
    try:
        procmon_loop()
    except Exception as e:
        print e
        

